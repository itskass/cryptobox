# Requirements
- https - Coinbase webhooks require HTTPS
- Enabled Less secure app access - Mailer requies https://myaccount.google.com/lesssecureapps to send emails from your account
- Domain name (Or you can use self signed certificate)

# Installing

1. Install Golang 
`https://golang.org/doc/install` make sure you setup the path correctly test by running `go version`

2. Clone the repo 
`git clone https://gitlab.com/itskass/cryptobox`

3. Install
`go install .` inside folder

# Setup Coinbase commerce

1. Create a Checkout in coinbase, make a note of the checkout id located at end of URL
https://commerce.coinbase.com/checkout/**18e673a3-e1cf-4157-ac44-23eddaf3af1b**

2. Register your `/comfirmed` webhook on coinbase, [https://commerce.coinbase.com/dashboard/settings](https://commerce.coinbase.com/dashboard/settings)
`endpoint: https://your-server.com/comfirmed`. Click details and ensure only `charge:confirmed` is checked.

# Setup Drive API

1. Click Enable drive API here https://developers.google.com/drive/api/v3/quickstart/go 
and download credentials.json to the cryptobox root folder (the one containing main.go).

2. Go to your google drive and right click the files you wish to give access to, and press get sharable link, make note of the fileID from it. e.g. https://docs.google.com/spreadsheets/d/*1yZv9w9zRKwrGTaR-YzmAqMefw4wMlaXocejdxZaTs6w*/edit?usp=sharing

# Configuring Cryptobox

1. Modify the config.json inside cryptobox root folder. Setting your Email address and password as well as SMTP and SMTP Server address (by default values set for GMAIL).

2. Add your product to products array, inputing the coinbase checkout id and drive file id you made a note of. 

# Setup SSL 

1. Install NGINX `sudo apt install nginx`

2. Configure nginx `sudo nano /etc/nginx/sites-available/default` replace server_name location with 
```
    server_name mydomain.com www.mydomain.com;

    location / {
        proxy_pass http://localhost:8080;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }
```

3. Check its working `sudo nginx -t` then restart the service `sudo service nginx restart`.

4. Install and run Certbot: `sudo apt-get install python-certbot-nginx` and 
    `sudo certbot --nginx -d mydomain.com -d www.mydomain.com`

# Run

From inside the root folder run `go run main.go` you will also want to use nginx w/LetsEncrypt to enable https and route port :8080 to :443.

**First time you run, you will have to authenticate google, follow onscreen instructions**