package coinbase

import "encoding/json"

type Charge struct {
	ID          string                 `json:"id"`
	Resource    string                 `json:"resource"`
	Code        string                 `json:"code"`
	Name        string                 `json:"name"`
	Description string                 `json:"description"`
	LogoURL     string                 `json:"logo_url"`
	HostedURL   string                 `json:"hosted_url"`
	CreatedAt   string                 `json:"created_at"`
	ExpiresAt   string                 `json:"expires_at"`
	Timeline    []TimelineEvent        `json:"timeline"` // map[string]interface{}
	Metadata    map[string]string      `json:"metadata"` //map[string]string,
	PricingType string                 `json:"pricing_type" `
	Pricing     map[string]Money       `json:"pricing"`   //map[string]Money
	Payments    []interface{}          `json:"payments"`  //[]string,
	Addresses   map[string]interface{} `json:"addresses"` // map[string]string,
	RedirectUrl string                 `json:"redirect_url"`
	CancelUrl   string                 `json:"cancel_url"`
	Checkout    map[string]string      `json:"checkout"`
}

type Money struct {
	Amount   string `json:"amount"`
	Currency string `json:"currency"`
}

type TimelineEvent struct {
	Time   string `json:"time"`
	Status string `json:"status"`
}

type Event struct {
	// ID: Event UUID
	ID string `json:"id"`
	// Resource name: "event"
	Resource string `json:"resource"`
	// Type: event type: charge:created, charge:confirmed, charge:failed, charge:delayed, charge:pending
	Type string `json:"type"`
	// CreatedAt: Event creation time
	CreatedAt string `json:"created_at"`
	// APIVersion: API version of the data payload
	APIVersion string `json:"api_version"`
	// Data: Event payload: resource of the associated object (e.g. charge) at the time of the event
	Data map[string]interface{} `json:"data"`
}

func (c Charge) JSON() string {
	buf, _ := json.Marshal(c)
	return string(buf)
}
