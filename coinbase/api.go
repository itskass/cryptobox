package coinbase

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
)

type API struct {
	Key string
	URL string
}

type ChargeRequest struct {
	Name        string            `json:"name"`
	Description string            `json:"description"`
	PricingType string            `json:"pricing_type"`
	LocalPrice  Money             `json:"local_price"`
	Metadata    map[string]string `json:"metadata"`
	RedirectUrl string            `json:"redirect_url"`
	CancelUrl   string            `json:"cancel_url"`
}

func (a *API) Header() http.Header {
	return http.Header{
		"X-CC-Api-Key": []string{a.Key},
		"X-CC-Version": []string{"2018-03-22"},
	}
}

func (a *API) CreateCharge(request *ChargeRequest) (*Charge, error) {
	if request.Name == "" {
		return nil, errors.New("name is required")
	}
	if request.Description == "" {
		return nil, errors.New("name is required")
	}
	if request.PricingType == "" {
		return nil, errors.New("name is required")
	}

	buf, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	res, err := http.Post(a.URL+"/charges", "application/json", bytes.NewReader(buf))
	if err != nil {
		return nil, err
	}

	responseData := make(map[string]interface{})
	err = json.NewDecoder(res.Body).Decode(&responseData)
	if err != nil {
		return nil, err
	}

	return responseData["data"].(*Charge), nil
}
