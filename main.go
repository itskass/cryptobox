package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"net/smtp"

	"gitlab.com/itskass/cryptobox/handlers"
	"gitlab.com/itskass/cryptobox/mailer"
)

func main() {
	log.Println("Kass's CryptoBox")
	log.Println("- Loading config...")
	conf := readConfig("./config.json")
	log.Println(conf)

	log.Println("- Creating Mailing client ...")
	m, err := mailer.New(
		conf.Address,
		"credentials.json",
		conf.Server,
		smtp.PlainAuth("", conf.Address, conf.Password, conf.STMP),
	)
	if err != nil {
		panic(err)
	}
	log.Println("- Creating Product Request handler ..")
	handler := handlers.New(conf.Products, m)

	log.Println("Running Kass's CryptoBox...")
	log.Println("- Handling requests")

	// register handlers
	http.HandleFunc("/confirmed", handler.Func)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Cryptobox running"))
	})

	http.HandleFunc("/test-email", func(w http.ResponseWriter, r *http.Request) {
		m.Send(m.Address, "Test Email", "Mailing client setup successfully!")
		log.Println("Test email sent to", m.Address)
		w.WriteHeader(200)
	})

	log.Fatal(http.ListenAndServe(":8080", nil))
}

func readConfig(path string) *handlers.Config {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}

	conf := &handlers.Config{}
	if err := json.Unmarshal(b, conf); err != nil {
		panic(err)
	}

	return conf
}
