package handlers

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/itskass/cryptobox/coinbase"
	"gitlab.com/itskass/cryptobox/mailer"
)

type Cryptobox struct {
	Products map[string]*Product
}

func New(products []*Product, m *mailer.Mailer) *Cryptobox {
	c := &Cryptobox{Products: make(map[string]*Product)}
	// add products
	for _, p := range products {
		p.Mailer = m
		c.Products[p.CheckoutID] = p
	}

	return c
}

func (c *Cryptobox) Func(w http.ResponseWriter, r *http.Request) {
	body := make(map[string]interface{})
	err := json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		log.Println("bad request", r.Body)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	event := &coinbase.Event{}
	if err := _mapToStruct(body["event"], event); err != nil {
		log.Println("couldnt parse event data", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if event.Type != "charge:confirmed" {
		log.Println("only accepts charge confirmed", event.ID, event.Type)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	charge := &coinbase.Charge{}
	if err := _mapToStruct(event.Data, charge); err != nil {
		log.Println("couldnt parse charge data", event.Data)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	checkoutID := charge.Checkout["id"]
	product, ok := c.Products[checkoutID]
	if !ok {
		log.Println("couldn't find matching checkout", checkoutID)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if err := product.OnChargeConfirmed(charge); err != nil {
		log.Println("failed charge:", charge.ID, err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func _mapToStruct(src interface{}, dest interface{}) error {
	buf := new(bytes.Buffer)
	json.NewEncoder(buf).Encode(&src)
	return json.NewDecoder(buf).Decode(dest)
}
