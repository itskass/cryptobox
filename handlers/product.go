package handlers

import (
	"strings"

	"github.com/google/uuid"
	"gitlab.com/itskass/cryptobox/coinbase"
	"gitlab.com/itskass/cryptobox/mailer"
)

type Product struct {
	CheckoutID string         `json:"checkout_id"`
	FileID     string         `json:"file_id"`
	Mailer     *mailer.Mailer `json:"mailer"`
	Name       string         `json:"name"`
	Website    string         `json:"website"`
	Team       string         `json:"team"`
}

func (p *Product) OnChargeConfirmed(charge *coinbase.Charge) error {
	name := charge.Metadata["name"]
	email := charge.Metadata["email"]
	code := uuid.New().String()

	// notify seller
	p.Mailer.Send(p.Mailer.Address, "Purchase:"+p.Name+" - "+email, charge.JSON())

	// If Recipient contains
	if !strings.Contains(email, "@gmail") {
		return p.sendGmailRequired(name, email, code)
	}

	// Invite to drive
	return p.sendDriveInvitation(name, email, code)
}

func (p *Product) sendGmailRequired(name, email, code string) error {
	msg := `
		<h1>Thanks for purchasing '` + p.Name + `'!</h1>
		<p>
			<b>Google Account Required.</b>
			We tried to invite you to the the Google Drive containing '` + p.Name + `', but we can only invite
			google accounts (e.g. email addresses ending in @gmail.com).
			<br>

			Please email us from a gmail account referencing: <code>` + code + `</code>
		</p>
		<p>
			Regards,<br/>
			` + p.Team + `
		</p>
		<small><a href="` + p.Website + `">` + p.Website + `</a></small>
	`

	// send email to customer
	if err := p.Mailer.Send(email, p.Name+" - Purchased | "+p.Website, msg); err != nil {
		return err
	}

	// send email to owner
	return p.Mailer.Send(p.Mailer.Address, email+"- Gmail Required: "+p.Name, code)
}

func (p *Product) sendDriveInvitation(name, email, code string) error {
	//fileURL := "google.drive.com/view?" + p.FileID
	msg := `
		<h1>Thanks for purchasing '` + p.Name + `'!</h1>
		<p>'
			Check your inbox for invitation to veiw the file. <br/>
			Purchase reference: <code>` + code + `</code>
		</p>
		<p>
			Regards,<br/>
			` + p.Team + `
		</p>
		<small><a href="` + p.Website + `">` + p.Website + `</a></small>
	`

	// Invite to drive
	if err := p.Mailer.GoogleDriveInvitation(p.FileID, email, p.Name); err != nil {
		return err
	}

	return p.Mailer.Send(email, p.Name+" - Purchased | "+p.Website, msg)
}
