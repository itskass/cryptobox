package handlers

type Config struct {
	Address  string     `json:"address"`
	Password string     `json:"password"`
	STMP     string     `json:"smtp"`
	Server   string     `json:"server"`
	Products []*Product `json:"products"`
}
