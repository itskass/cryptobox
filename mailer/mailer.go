package mailer

import (
	"log"
	"net/smtp"

	"google.golang.org/api/drive/v3"
)

type Mailer struct {
	Address string
	Server  string
	Auth    smtp.Auth
	Drive   *drive.Service
}

// New Creates new mailer
func New(addr, credentialsFile, server string, auth smtp.Auth) (*Mailer, error) {
	// create authorized http client for google drive
	driveClient, err := newDriveClient(credentialsFile)
	if err != nil {
		return nil, err
	}

	// create drive service helper
	drive, err := drive.New(driveClient)
	if err != nil {
		return nil, err
	}

	return &Mailer{
		Address: addr,
		Drive:   drive,
		Auth:    auth,
		Server:  server,
	}, nil

}

func (m *Mailer) Send(to, subject, body string) error {
	msg := "From: " + m.Address + "\n" +
		"To: " + to + "\n" +
		"Subject:" + subject + "\n" +
		"Mime-Version: 1.0;\n" +
		"Content-Type: text/html; charset=\"ISO-8859-1\";\n" +
		"Content-Transfer-Encoding: 7bit;\n\n" +
		body

	err := smtp.SendMail(
		m.Server,
		m.Auth,
		m.Address,
		[]string{to},
		[]byte(msg),
	)

	if err != nil {
		log.Println("failed to send email", err)
	}

	log.Println("sending email to:", to)
	return nil
}

func (m *Mailer) GoogleDriveInvitation(fileID, to, name string) error {

	permissionCreateCall := m.Drive.Permissions.Create(fileID, &drive.Permission{
		EmailAddress: to,
		Role:         "reader",
		Type:         "user",
	})

	_, err := permissionCreateCall.Do()
	return err

}
